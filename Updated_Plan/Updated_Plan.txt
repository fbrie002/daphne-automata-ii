Current situation:

- I have created a pedestal to display the hand, now it's time to figure out how to pass the wires below it and connect everything. I have made a couple of holes on the wooden surface of the pedestal (it was hard without a drill but i did it manually wooo!)

- Markov chain code is fixed. OF/Arduino communication not clear yet :(

- I connected a stripped USB wire to Arduino as second 5V source of power and now the lED matrix works hurray!



I need to:

- Solder the LEDs to longer wires so that they can connect to Arduino underneath the surface of the plinth.

- Connect LEDs to Arduino.

- Forage laurel leaves to attach to the back of the hand.

- Assemble the final structure!

- I have started writing the documentation and research, now need to start video and photo gathering for final documentation media.


Wednesday 27th May 2020