I have been trying to figure out how to send the string obtained from my Markov function code to Arduino to be displayed on the LED matrix in real time. I have been able to establish communication between OF and Arduino but I have failed to understand how to properly implement the rest.
Using the serialExample code I established a Serial communication, but the characters were being sent in the wrong direction for my code to work as I wanted to. Arduino was sending chars to OF to be displayed on the screen, instead of OF sending strings to be displayed on Arduino's Serial Monitor. 
To understand how to control Arduino with OF I had a look at the Firmata example and also downloaded ofxSerial because it sounded like it might be the more stable option, especially since I was looking to send quite a few char and wanted to avoid any loss of text.

Unfortunately I failed to understand how to work out this issue in the time that I had left. The project was finished but I had wanted to give this option a go one last time. In the future I would like to be able to understand how to integrate this type of communication successfully and hope to upgrade my project once I do. 
 

Thursday 4th June 2020