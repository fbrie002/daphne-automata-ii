

#include "utils.h"
#include <string>
#include <vector> 
#include <iostream> 
#include <fstream>
#include <map>
#include <random>
#include <time.h>       /* time */

std::string readFile(std::string filename)
{
    std::string line;
    std::string all_lines;
//    std::cout<<"all lines: "<<all_lines<<std::endl;
    std::ifstream myfile (filename);
    if (myfile.is_open())
    {
        while ( std::getline (myfile, line) )
        {
            all_lines += line + '\n';
        }
        myfile.close();
        
    }
    else std::cout<<"unable to open file"<<std::endl;
    
    return all_lines;
}
int main()
{
    
    std::string donna = readFile("cyborg.txt");
    std::string ovid = readFile("ovid.txt");
    std::string data = donna + ovid;
    
    std::vector <std::string> tokens =  tokenise(data, ' ');
//    for (std::string token : tokens)
//    {
//        std::cout << token << std::endl;
//    }
    
    // build the markov from the tokens

    std::map <std::string,std::string> markov; 

    srand (time(NULL));
//    std::cout << "rseed is: " << time(NULL) << std::endl;
    
    markov = addWordsToMarkov(markov, data);
    std::vector <std::string> keys = getMapKeys(markov);

       std::cout<<"keys.size is: "<<keys.size()<<std::endl;
    
    int index = randInt(keys.size()); //index is a random integer (which is max keys.size)

//    std::cout<<"index is: "<< index<<std::endl;
    
    std::string key = keys[index];
    
    std::cout<<"(start) key: '"<< key <<"';  options: "<<keys.size()<<";  chose: " <<index<<";"<<std::endl;
    
    for (auto i = 0; i < 500; ++i)
    {
        std::string options = markov[key];
        
        std::vector<std::string> options_vector = tokenise(options, ' ');
        int index = randInt(options_vector.size());

        std::string chosen_word = options_vector[index];

        //std::cout << "key: " << key << " options: " << options_vector.size() << " chose: " << index << "word " << options_vector[index] << std::endl;

        key = chosen_word; 
        std::cout<< chosen_word << " " << std::endl;

        std::string word = options_vector[index];
        //std::cout << word << std::endl;
    }

}
