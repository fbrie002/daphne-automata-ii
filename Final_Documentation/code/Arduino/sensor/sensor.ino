#include <CapacitiveSensor.h>
CapacitiveSensor capSensor = CapacitiveSensor(4,2);        // 10M resistor between pins 4 & 2, pin 2 is sensor pin, add a wire and or foil if desired
int threshold = 400;
const int ledPin12 = 12;
const int ledPin11 = 11;


//  
void setup() {
Serial.begin(9600);
pinMode(ledPin12, OUTPUT);
pinMode(ledPin11, OUTPUT);
//pinMode(ledPin10, OUTPUT);
}
//
void loop() {
long sensorValue = capSensor.capacitiveSensor(30);
Serial.println(sensorValue);
//
if(sensorValue > threshold){
  digitalWrite(ledPin12, HIGH);
  digitalWrite(ledPin11, HIGH);

} else{
  digitalWrite(ledPin12, LOW);
  digitalWrite(ledPin11, LOW);

}
//
delay(10);
//
}

//modify so that leds are switched on and off by touch (decided to not use the following version)

//#include <CapacitiveSensor.h>
//CapacitiveSensor capSensor = CapacitiveSensor(4, 2);
//int threshold = 50;
//const int ledPin12 = 12;
////const int ledPin11 = 11;
////const int ledPin10 = 10;
//bool LEDState = false; // start with assuming LEDs are off
//
//void setup() {
//  Serial.begin(9600);
//  pinMode(ledPin12, OUTPUT);
////  pinMode(ledPin11, OUTPUT);
////  pinMode(ledPin10, OUTPUT);
//}
//
//void loop() {
//  long sensorValue = capSensor.capacitiveSensor(30);
//  Serial.println(sensorValue);
//  if (sensorValue > 50) {
//    if (LEDState == true) {
//      digitalWrite(ledPin12, LOW); //turn off LEDs
////      digitalWrite(ledPin11, LOW); //turn off LEDs
////      digitalWrite(ledPin10, LOW); //turn off LEDs
//    }
//    else {
//      digitalWrite(ledPin12, HIGH); //turn on LEDs
////      digitalWrite(ledPin11, HIGH); //turn on LEDs
////      digitalWrite(ledPin10, HIGH); //turn on LEDs
//    }
//    LEDState = !LEDState;
//  }
//
//  // Give user a chance to let go
//  delay(1000);
//}
