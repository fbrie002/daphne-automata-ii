

#include "utils.h"
#include <string>
#include <vector> 
#include <iostream> 
#include <fstream>
#include <map>
#include <random>
#include <time.h>       /* time */

std::string readFile(std::string filename)
{
    std::string line;
    std::string all_lines;
    std::ifstream myfile (filename);
    if (myfile.is_open())
    {
        while ( std::getline (myfile,line) )
        {
            all_lines += line + '\n';
        }
        myfile.close();
    }
    return all_lines;
}
int main()
{

    std::string ovid = readFile("ovid.txt");
    std::string donna = readFile("cyborg.txt");
    std::string data = ovid + donna;
   //std::cout << data; 
    std::vector <std::string> tokens =  tokenise(data, ' ');
    for (std::string token : tokens)
    {
        std::cout << token << std::endl;
    }
    
    // build the markov from the tokens

    std::map <std::string,std::string> markov; 

    srand (time(NULL));
    std::cout << "rseed: " << time(NULL) << std::endl;
    
//    std::cout<<data<<std::endl;
    
    markov = addWordsToMarkov(markov, data);
    std::vector <std::string> keys = getMapKeys(markov);
    
    std::cout<<keys.size()<<std::endl;
    
    //keys is the problem! it's empty (either markov not working or used improperly or key initialised wrong or text not uploading?
    
//    int index = randInt(keys.size()); //when keys.size is used as the max int variable it always returns 0
    int index = randInt(200); //which is whi i'm hard coding here to experiment
    std::cout<<index<<std::endl;
    std::string key = keys[index];
    
    std::cout << "Random start key: " << key << " options: " << keys.size() << " chose: " << index << std::endl;
    
    for (auto i = 0; i < 100; ++i)
    {
        
        std::string options = markov[key];
        
        std::vector<std::string> options_vector = tokenise(options, ' ');
        int index = randInt(options_vector.size());

        std::string chosen_word = options_vector[index];

        //std::cout << "key: " << key << " options: " << options_vector.size() << " chose: " << index << "word " << options_vector[index] << std::endl;

        key = chosen_word; 
        std::cout<< chosen_word << " " << std::endl;

        std::string word = options_vector[index];
        //std::cout << word << std::endl;
    }

}
