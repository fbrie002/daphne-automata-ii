#include <CapacitiveSensor.h>
CapacitiveSensor capSensor = CapacitiveSensor(4,2);        // 10M resistor between pins 4 & 2, pin 2 is sensor pin, add a wire and or foil if desired
  int threshold = 400;
  const int ledPin = 12;
  
void setup() {
Serial.begin(9600);
pinMode(ledPin, OUTPUT);
}

void loop() {
long sensorValue = capSensor.capacitiveSensor(30);
Serial.println(sensorValue);

if(sensorValue > threshold){
  digitalWrite(ledPin, HIGH);
} else{
  digitalWrite(ledPin, LOW);
}

delay(10);

}
